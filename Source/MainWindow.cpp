#include "MainWindow.h"

//==============================================================================
MainAppWindow::MainAppWindow() :
    DocumentWindow (JUCEApplication::getInstance()->getApplicationName(),
                    Colours::lightgrey, DocumentWindow::allButtons),
    mainComponent (nullptr)
    
{
    mainComponent = new MainComponent();

    centreWithSize ((int) ((float) getParentWidth() * 0.6f),
                    (int) ((float) getParentHeight() * 0.8f));
    setVisible (true);

    mainComponent->setBounds (0, 0, getWidth(), getHeight());
    setContentOwned (mainComponent, true);
}

MainAppWindow::~MainAppWindow()
{
}

void MainAppWindow::closeButtonPressed()
{
    JUCEApplication::getInstance()->systemRequestedQuit();
}