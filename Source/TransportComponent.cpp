#include "TransportComponent.h"

//==============================================================================
const int TransportComponent::offset = 10;
const int TransportComponent::width  = 60;
const int TransportComponent::height = 30;

//==============================================================================
TransportComponent::TransportComponent() :
    transport (nullptr),
    songAudioClip (nullptr),
    play ("Play"),
    stop ("Stop"),
    load ("Load"),
    songName ("(None)"),
    thumbnailCache (5),
    sampleBuffer (2, 1024),
    songLengthSeconds (0.0),
    thumbnail (nullptr)
{
    load.addListener (this);
    play.addListener (this);
    stop.addListener (this);

    load.setColour (juce::TextButton::buttonColourId, juce::Colour ((uint8)0, (uint8)128, (uint8)232));
    play.setColour (juce::TextButton::buttonColourId, juce::Colour ((uint8)44, (uint8)193, (uint8)0));
    stop.setColour (juce::TextButton::buttonColourId, juce::Colour ((uint8)232, (uint8)0, (uint8)0));

    this->addAndMakeVisible (&load);
    this->addAndMakeVisible (&play);
    this->addAndMakeVisible (&stop);
    this->addAndMakeVisible (&logoComponent);

    thumbnail = new juce::AudioThumbnail (512, kreator.getFormatManager(), thumbnailCache);

    juce::String logoImagePath = juce::File::getSpecialLocation (juce::File::SpecialLocationType::currentExecutableFile).getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getFullPathName() + "\\Source\\";
    logo = juce::ImageCache::getFromFile (juce::File (logoImagePath + "musitkv4.png"));
    logoComponent.setImage (logo);
}

TransportComponent::~TransportComponent()
{
    transport->stop();
    transport->setAudioSource();

    thumbnail = nullptr;
    songAudioClip = nullptr;
}

//==============================================================================
void TransportComponent::paintClip (juce::Graphics& g)
{
    //Setup the thumbnail position and area:
    const int thumbWidth = (this->getWidth() - stop.getBounds().getX() - stop.getBounds().getWidth()) - (offset * 2);
    thumbnailPos.setBounds (stop.getX() + stop.getWidth() + offset, offset - 2, thumbWidth, getHeight() - 6 - offset - 2);

    //Paint the thumbnail:
    juce::ColourGradient gradient (Colours::lightgrey, 0.0f, (float)getHeight(), Colour::greyLevel (0.95f), 0.0f, 0.0f, false);

    g.setGradientFill (gradient);
    g.fillAll();

    g.setColour (juce::Colour ((uint8)200, (uint8)0, (uint8)0, (uint8)200));

    if (thumbnail->getTotalLength() > 0)
    {
        thumbnail->drawChannels (g, thumbnailPos, 0.0, songLengthSeconds, 1.0f);
    }

    //Paint the thumbnail rect:
    g.setColour (juce::Colour (0xcc555555));
    g.drawRect (thumbnailPos);

    //Paint the song name:
    g.setFont (10.0f);
    g.setColour (Colours::black.withAlpha (0.1f));

    const int textWidth = g.getCurrentFont().getStringWidth (songName);

    const juce::Rectangle<float> thumbPos (thumbnailPos.toFloat());
    juce::Rectangle<float> r (2.0f + thumbPos.getX(),
                              2.0f + thumbPos.getY(),
                              juce::jmin ((float) textWidth, thumbPos.getWidth() - 2.0f),
                              16.0f);

    g.fillRoundedRectangle (r, 3);
    r.reduce (2, 2);

    g.setColour (Colours::black);
    g.drawFittedText (songName, (int)r.getX(), (int)r.getY(), (int)r.getWidth(), (int)r.getHeight(), juce::Justification::topLeft, 1, 0.7f);
}

//==============================================================================
void TransportComponent::paint (juce::Graphics& g)
{
    const float height = (float) getHeight();

    juce::ColourGradient gradient (juce::Colours::darkgrey, 0.0f, 0.0f, juce::Colours::lightgrey, 0.0f, height, false);
    g.setGradientFill (gradient);

    g.fillRect (0, 0, getWidth(), getHeight());

    paintClip (g);

    g.setColour (juce::Colour (0xcc555555));
    g.fillRect (logoComponent.getBounds().expanded (2, 2));
}

void TransportComponent::resized()
{
    load.setBounds (offset, offset, width, height);
    play.setBounds (width + offset*2, offset, width, height);
    stop.setBounds (width*2 + offset*3, offset, width, height);

    static const int imageWidth = width*3 + offset*2;
    const int imageHeight       = getHeight() - (stop.getHeight() + offset*2) - offset;
    Rectangle<int> logoBounds (offset, stop.getHeight() + offset*2, imageWidth, imageHeight);
    logoComponent.setBounds (Rectangle<int> (offset, stop.getHeight() + offset*2, imageWidth, imageHeight).reduced (2, 2));
}

//==============================================================================
void TransportComponent::buttonClicked (juce::Button* button)
{
    jassert (transport != nullptr);

    if (button == &play)
    {
        transport->start();
    }
    else if (button == &stop)
    {
        transport->stop();
    }
    else if (button == &load)
    {
        juce::FileChooser chooser (juce::String ("Load an audio file"), File::getSpecialLocation (File::userDesktopDirectory), kreator.getExtensionsAsString(), true);
        juce::File soundFile;

        if (chooser.browseForFileToOpen() == true)
        {
            soundFile = chooser.getResult();

            if (soundFile.existsAsFile() == true)
            {
                transport->stop();
                transport->setAudioSource();

                songAudioClip = kreator.createSoundClipFor (soundFile);
                transport->setAudioSource (songAudioClip, true);

                songName = soundFile.getFileName();

                //Get the reader:
                juce::AudioFormatReader* reader = songAudioClip->getAudioFormatReader();

                //Setup the thumbnail:
                thumbnail->clear();
                thumbnail->reset (reader->numChannels, reader->sampleRate, reader->lengthInSamples);
                songLengthSeconds = reader->lengthInSamples / reader->sampleRate;

                sampleBuffer.setSize ((int) reader->numChannels, (int) reader->lengthInSamples);

                reader->read (&sampleBuffer, 0, (int)reader->lengthInSamples, 0, true, true);

                thumbnail->addBlock (0, sampleBuffer, 0, sampleBuffer.getNumSamples());

                resized();
                repaint();
            }
        }
    }
}