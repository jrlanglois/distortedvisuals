#ifndef TRANSPORT_H
#define TRANSPORT_H

#include "SoundClip.h"

class Transport : public juce::AudioProcessor
{
public:
    Transport();

    ~Transport();

    //==============================================================================
    float getCurrentAmplitude();

    //==============================================================================
    /** Starts playing (if a source has been selected).

        If it starts playing, this will send a message to any ChangeListeners
        that are registered with this object.
    */
    void start();

    /** Stops playing.

        If it's actually playing, this will send a message to any ChangeListeners
        that are registered with this object.
    */
    void stop();

    /** Returns true if it's currently playing. */
    bool isPlaying() const noexcept;

    /** Returns true if it's looping. */
    bool isLooping() const noexcept;

    //==============================================================================
    /** Sets the reader that is being used as the input source.

        This will stop playback, reset the position to 0 and change to the new reader.

        The source passed in will not be deleted by this object, so must be managed by
        the caller.

        @param newSource the new input source to use. This may be zero
    */
    void setAudioSource (SoundClip* newSource = nullptr, const bool loopPlay = false);

    //==============================================================================
    /** Changes the gain to apply to the output.

        @param newGain  a factor by which to multiply the outgoing samples,
                        so 1.0 = 0dB, 0.5 = -6dB, 2.0 = 6dB, etc.
    */
    void setGain (float newGain) noexcept;

    /** Returns the current gain setting.

        @see setGain
    */
    float getGain() const noexcept;

    //==============================================================================
    /** @internal */
    const juce::String getName() const;
    /** @internal */
    void prepareToPlay (double sampleRate,  int estimatedSamplesPerBlock);
    /** @internal */
    void releaseResources();
    /** @internal */
    void processBlock (juce::AudioSampleBuffer& buffer, juce::MidiBuffer& midiMessages);
    /** @internal */
    const juce::String getInputChannelName (int channelIndex) const;
    /** @internal */
    const juce::String getOutputChannelName (int channelIndex) const;
    /** @internal */
    bool isInputChannelStereoPair (int index) const;
    /** @internal */
    bool isOutputChannelStereoPair (int index) const;
    /** @internal */
    bool silenceInProducesSilenceOut() const;
    /** @internal */
    bool acceptsMidi() const;
    /** @internal */
    bool producesMidi() const;
    /** @internal */
    juce::AudioProcessorEditor* createEditor();
    /** @internal */
    bool hasEditor() const;
    /** @internal */
    int getNumParameters();
    /** @internal */
    const juce::String getParameterName (int parameterIndex);
    /** @internal */
    float getParameter (int parameterIndex);
    /** @internal */
    const juce::String getParameterText (int parameterIndex);
    /** @internal */
    void setParameter (int parameterIndex, float newValue);
    /** @internal */
    int getNumPrograms();
    /** @internal */
    int getCurrentProgram();
    /** @internal */
    void setCurrentProgram (int index);
    /** @internal */
    const String getProgramName (int index);
    /** @internal */
    void changeProgramName (int index, const juce::String& newName);
    /** @internal */
    void getStateInformation (juce::MemoryBlock& destData);
    /** @internal */
    void setStateInformation (const void* data, int sizeInBytes);

private:
    //==============================================================================
    juce::AudioTransportSource transportSource;

    float amplitude;
                
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Transport);
};

#endif //TRANSPORT_H