#ifndef SOUND_CLIP_CREATOR_H
#define SOUND_CLIP_CREATOR_H

#include "SoundClip.h"

class SoundClipCreator
{
public:
    SoundClipCreator();
    ~SoundClipCreator();

    //==============================================================================
    static const juce::StringArray getExtensions();
    static const juce::String getExtensionsAsString();
    static const bool hasValidExtension (const juce::String& extension);
    static const bool hasValidExtension (const juce::File& possibleSoundFile);

    //==============================================================================
    SoundClip* createSoundClipFor (juce::String& path);
    SoundClip* createSoundClipFor (juce::File& possibleSoundFile);

    //==============================================================================
    juce::AudioFormatManager& getFormatManager() { return formatManager; }

private:
    //==============================================================================
    juce::AudioFormatManager formatManager;

    //==============================================================================
    juce::AudioFormatReader* createReader (const juce::File& possibleSoundFile);

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SoundClipCreator);
};

#endif //SOUND_CLIP_CREATOR_H