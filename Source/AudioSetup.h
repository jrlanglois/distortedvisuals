#ifndef AUDIO_SETUP_H
#define AUDIO_SETUP_H

#include "SoundClipCreator.h"
#include "Transport.h"

class AudioSetup
{
public:
    AudioSetup();
    ~AudioSetup();

    //==============================================================================
    int addAndConnectTransport (Transport*& transport);

private:
    //==============================================================================
    juce::AudioDeviceManager        deviceManager;
    juce::AudioProcessorPlayer      player;
    juce::AudioProcessorGraph       graph;

    juce::AudioPluginInstance*      audioOutput;
    int                             outputID;

    //==============================================================================
    juce::StringArray getListOfDevices (const bool giveMeInputDevices = false);
    void initialize();
    void shutdown();

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (AudioSetup);
};

#endif //AUDIO_SETUP_H