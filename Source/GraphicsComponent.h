#ifndef GRAPHICS_COMPONENT_H
#define GRAPHICS_COMPONENT_H

#include "../JuceLibraryCode/JuceHeader.h"

class GraphicsComponent : public juce::Component,
					  public juce::OpenGLRenderer
{
public:
    GraphicsComponent();
    ~GraphicsComponent();

    //==============================================================================
    void resized();

    //==============================================================================
    void loop();

    //==============================================================================
    void newOpenGLContextCreated();

    void openGLContextClosing();

    void renderOpenGL();

	void update();

	void setScale (float newScale);

private:
    //==============================================================================
    juce::OpenGLContext         context;
	juce::ScopedPointer<juce::OpenGLShaderProgram>   shader;
	juce::ScopedPointer<juce::OpenGLShaderProgram>   quadShader;

	float* vertices;
	float* normals;
	float rotation;

	float* quadVertices;

	unsigned int frameBuffer;

	unsigned int renderBuffer;

	// the image is loaded into this before being sent to GL
	unsigned char *image;

	// texture handle
	unsigned int texture;

	float scale;
	float scale2;
	float scale3;
	float scale4;

	bool resizedAlready;
	
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (GraphicsComponent)
};

#endif //GRAPHICS_COMPONENT_H