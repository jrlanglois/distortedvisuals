#include "SoundClip.h"

SoundClip::SoundClip (juce::AudioFormatReader* sourceReader) :
    juce::AudioFormatReaderSource (sourceReader, true)
{
    jassert (sourceReader != nullptr); //Incompatible file type!
}

SoundClip::~SoundClip()
{
}