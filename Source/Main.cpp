#include "MainWindow.h"

//==============================================================================
class DistortedVisualsApplication : public juce::JUCEApplication
{
public:
    //==============================================================================
    DistortedVisualsApplication()
    {
    }

    ~DistortedVisualsApplication()
    {
    }

    //==============================================================================
    void initialise (const String& /*commandLine*/)
    {
        mainWindow = new MainAppWindow();
    }

    void shutdown()
    {
        mainWindow = nullptr;
    }

    //==============================================================================
    void systemRequestedQuit()
    {
        quit();
    }

    //==============================================================================
    const String getApplicationName()
    {
        return ProjectInfo::projectName;
    }

    const String getApplicationVersion()
    {
        return ProjectInfo::versionString;
    }

    bool moreThanOneInstanceAllowed()
    {
        return true;
    }

    void anotherInstanceStarted (const String& /*commandLine*/)
    {
    }

private:
    ScopedPointer<MainAppWindow> mainWindow;
};

//==============================================================================
// This macro generates the main() routine that starts the app.
START_JUCE_APPLICATION(DistortedVisualsApplication)