#include "SoundClipCreator.h"

SoundClipCreator::SoundClipCreator()
{
    formatManager.registerBasicFormats();
}

SoundClipCreator::~SoundClipCreator()
{
}

//==============================================================================
const juce::StringArray SoundClipCreator::getExtensions()
{
    juce::StringArray ext;
    ext.add (".aac");
    ext.add (".aiff");
    ext.add (".caf");
    ext.add (".flac");
    ext.add (".ogg");
    ext.add (".wav");

    ext.sort (true);
    ext.minimiseStorageOverheads();

    return ext;
}

const juce::String SoundClipCreator::getExtensionsAsString()
{
    juce::StringArray arr   = getExtensions();
    juce::String result     = "";

    for(int i = 0; i < arr.size(); ++i)
    {
        result += "*";
        result += arr[i];
        result += ";";
    }

    return result;
}

const bool SoundClipCreator::hasValidExtension (const juce::String& extension)
{
    juce::StringArray arr = getExtensions();

    for (int i = 0; i < arr.size(); ++i)
    {
        if (extension == arr[i]) { return true; }
    }

    return false;
}

const bool SoundClipCreator::hasValidExtension (const juce::File& possibleSoundFile)
{
    return hasValidExtension (possibleSoundFile.getFileExtension());
}

//==============================================================================
juce::AudioFormatReader* SoundClipCreator::createReader (const juce::File& possibleSoundFile)
{
    if (hasValidExtension (possibleSoundFile.getFileExtension()))
    {
        juce::AudioFormatReader* afr = formatManager.createReaderFor (possibleSoundFile);
        jassert (afr != nullptr); //Invalid file type!

        return afr;
    }

    return nullptr;
}

//==============================================================================
SoundClip* SoundClipCreator::createSoundClipFor (juce::String& path)
{
    jassert (path.isNotEmpty())
    jassert (juce::File::isAbsolutePath (path))

    juce::File soundFile (path);

    return createSoundClipFor (soundFile);
}

SoundClip* SoundClipCreator::createSoundClipFor (juce::File& possibleSoundFile)
{
    jassert (possibleSoundFile.exists());
    jassert (possibleSoundFile.existsAsFile());

    return new SoundClip (createReader (possibleSoundFile));
}