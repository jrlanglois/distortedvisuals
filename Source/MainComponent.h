#ifndef MAIN_COMPONENT_H
#define MAIN_COMPONENT_H

#include "AudioSetup.h"
#include "TransportComponent.h"
#include "GraphicsComponent.h"

class MainComponent : public juce::Component,
                      public juce::Timer
{
public:
    MainComponent();
    ~MainComponent();

    //==============================================================================
    void resized();
    void timerCallback();

private:
    //==============================================================================
    AudioSetup          setup;
    Transport*          transport;
    TransportComponent  transComp;

	GraphicsComponent opengl;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};

#endif //MAIN_COMPONENT_H