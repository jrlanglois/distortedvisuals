#ifndef TRANSPORT_COMPONENT_H
#define TRANSPORT_COMPONENT_H

#include "Transport.h"
#include "SoundClipCreator.h"

class TransportComponent : public juce::Component,
                        public juce::Button::Listener
{
public:
    TransportComponent();
    ~TransportComponent();

    //==============================================================================
    void paint (juce::Graphics& g);
    void resized();

    //==============================================================================
    void buttonClicked (juce::Button* button);

    //==============================================================================
    void setTransport (Transport* transport) { this->transport = transport; }

private:
    //==============================================================================
    juce::Image logo;
    juce::ImageComponent logoComponent;

    SoundClipCreator            kreator;
    Transport*                  transport;
    ScopedPointer<SoundClip>    songAudioClip;

    juce::TextButton    play;
    juce::TextButton    stop;
    juce::TextButton    load;
    juce::String        songName;

    ScopedPointer<juce::AudioThumbnail> thumbnail;
    juce::AudioSampleBuffer     sampleBuffer;
    juce::AudioThumbnailCache   thumbnailCache;
    double                      songLengthSeconds;
    juce::Rectangle<int>        thumbnailPos;

    //==============================================================================
    void paintClip (juce::Graphics& g);

    //==============================================================================
    static const int offset;
    static const int width;
    static const int height;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (TransportComponent);
};

#endif //TRANSPORT_COMPONENT_H