#version 120

varying vec3 normal;
varying vec3 position;

void main(void)
{
    vec3 colour = vec3 (1.0, 0.0, 0.0);

    vec3 light = vec3 (10.0, 10.0, -10.0);

    vec3 L = normalize (light - position);

    float intensity = max (dot (normalize (normal), L), 0.0);

    gl_FragColor = vec4 (intensity * 1.0, 0.0, 0.0, 1.0);
}