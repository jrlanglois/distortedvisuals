#ifndef __MAINWINDOW_H_776D77E__
#define __MAINWINDOW_H_776D77E__

#include "MainComponent.h"

//==============================================================================
class MainAppWindow : public juce::DocumentWindow
{
public:
    //==============================================================================
    MainAppWindow();
    ~MainAppWindow();

    void closeButtonPressed();

private:
    ScopedPointer<MainComponent> mainComponent;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainAppWindow);
};

#endif // __MAINWINDOW_H_776D77E__