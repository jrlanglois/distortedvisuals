#ifndef SOUND_CLIP_H
#define SOUND_CLIP_H

#include "../JuceLibraryCode/JuceHeader.h"

class SoundClip : public juce::AudioFormatReaderSource
{
public:
    SoundClip (juce::AudioFormatReader* sourceReader);
    ~SoundClip();

private:

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SoundClip);
};

#endif //SOUND_CLIP_H