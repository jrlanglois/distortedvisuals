#include "AudioSetup.h"

AudioSetup::AudioSetup()
{
    this->initialize();
}

AudioSetup::~AudioSetup()
{
    this->shutdown();
}

//==============================================================================
juce::StringArray AudioSetup::getListOfDevices (const bool giveMeInputDevices)
{
    ScopedPointer<AudioIODeviceType> type;

    #if (JUCE_WINDOWS)
        type = AudioIODeviceType::createAudioIODeviceType_WASAPI();
    #elif (JUCE_MAC)
        type = AudioIODeviceType::createAudioIODeviceType_CoreAudio();
    #else
        #error "Please configure an appropriate output device here for your platform!"
    #endif

    jassert (type != nullptr); //goobypls
    type->scanForDevices();

    return type->getDeviceNames (giveMeInputDevices);
}

//==============================================================================
void AudioSetup::initialize()
{
    //Initialize the device:
    deviceManager.initialise (0, 2, nullptr, true);

    //Configure the device:
    AudioDeviceManager::AudioDeviceSetup config;
    deviceManager.getAudioDeviceSetup (config);

    juce::StringArray arr   = getListOfDevices();
    config.outputDeviceName = arr[0]; //Set device to the first one available
    config.bufferSize       = 1024;

    deviceManager.setAudioDeviceSetup (config, true);

    //Add the audio processor player:
    deviceManager.addAudioCallback (&player);

    //Add the audio processor signal path to the processor player:
    player.setProcessor (&graph);

    //Add the transport processor to the processor graph:
    outputID = graph.addNode (audioOutput = new juce::AudioProcessorGraph::AudioGraphIOProcessor (juce::AudioProcessorGraph::AudioGraphIOProcessor::audioOutputNode))->nodeId;
}

void AudioSetup::shutdown()
{
    deviceManager.removeAudioCallback (&player);
    player.setProcessor (nullptr);
    graph.clear();
}

//==============================================================================
int AudioSetup::addAndConnectTransport (Transport*& transport)
{
    //Create the transport if it doesn't exist yet:
    if (transport == nullptr)
    {
        transport = new Transport();
    }

    //Add the transport processor to the processor graph:
    const int transportID = graph.addNode (transport)->nodeId;

    bool greatSuccess = graph.addConnection (transportID, 0, outputID, 0);
    greatSuccess &= graph.addConnection (transportID, 1, outputID, 1);
    jassert (greatSuccess); //NOPE - FAIL!

    return transportID;
}