#include "Transport.h"

Transport::Transport()
{
    setPlayConfigDetails (0, 2, 44100, 1024);
}

Transport::~Transport()
{
}

//==============================================================================
float Transport::getCurrentAmplitude()
{
    return amplitude;
}

//==============================================================================
void Transport::start()
{
    transportSource.start();
}

void Transport::stop()
{
    transportSource.setPosition (0.0);
    transportSource.stop();
}

bool Transport::isPlaying() const noexcept
{
    return transportSource.isPlaying();
}

bool Transport::isLooping() const noexcept
{
    return transportSource.isLooping();
}

//==============================================================================
void Transport::setAudioSource (SoundClip* newSource, const bool loopPlay)
{
    if (newSource != nullptr)
    {
        newSource->setLooping (loopPlay);
    }

    transportSource.setSource (newSource);
}

//==============================================================================
const juce::String Transport::getName() const
{
    return "Transport";
}

//==============================================================================
void Transport::prepareToPlay (const double sampleRate, const int estimatedSamplesPerBlock)
{
    transportSource.prepareToPlay (estimatedSamplesPerBlock, sampleRate);
    setPlayConfigDetails (0, 2, sampleRate, estimatedSamplesPerBlock);
}

void Transport::releaseResources()
{
    transportSource.releaseResources();
}

void Transport::processBlock (juce::AudioSampleBuffer& buffer, juce::MidiBuffer& /*midiMessages*/)
{
    if (transportSource.isPlaying())
    {
        juce::AudioSourceChannelInfo info (&buffer, 0, buffer.getNumSamples());
        transportSource.getNextAudioBlock (info);

        amplitude = 0.0f;

        for (int i = 0; i < buffer.getNumChannels(); ++i)
        {
            amplitude += buffer.getRMSLevel (i, 0, buffer.getNumSamples());
        }

        amplitude /= buffer.getNumChannels();
    }
    else
    {
        amplitude = 0.0f;
        buffer.clear();
    }
}

//==============================================================================
const juce::String Transport::getInputChannelName (const int /*channelIndex*/) const
{
    return "Some Channel";
}

const juce::String Transport::getOutputChannelName (const int /*channelIndex*/) const
{
    return "Some Channel";
}

bool Transport::isInputChannelStereoPair (const int /*index*/) const
{
    return false;
}

bool Transport::isOutputChannelStereoPair (const int /*index*/) const
{
    return true;
}

bool Transport::silenceInProducesSilenceOut() const
{
    return true;
}

bool Transport::acceptsMidi() const
{
    return false;
}

bool Transport::producesMidi() const
{
    return false;
}

//==============================================================================
juce::AudioProcessorEditor* Transport::createEditor()
{
    return nullptr;
}

bool Transport::hasEditor() const
{
    return false;
}

//==============================================================================
int Transport::getNumParameters()
{
    return 0;
}

const juce::String Transport::getParameterName (const int /*parameterIndex*/)
{
    return juce::String::empty;
}

float Transport::getParameter (const int /*parameterIndex*/)
{
    return 0.0f;
}

const juce::String Transport::getParameterText (const int /*parameterIndex*/)
{
    return juce::String::empty;
}

void Transport::setParameter (const int /*parameterIndex*/, const float /*newValue*/)
{
}

//==============================================================================
int Transport::getNumPrograms()
{
    return 0;
}

int Transport::getCurrentProgram()
{
    return 0;
}

void Transport::setCurrentProgram (const int /*index*/)
{
}

const juce::String Transport::getProgramName (const int /*index*/)
{
    return juce::String::empty;
}

void Transport::changeProgramName (const int /*index*/, const juce::String& /*newName*/)
{
}

//==============================================================================
void Transport::getStateInformation (juce::MemoryBlock& /*destData*/)
{
}

void Transport::setStateInformation (const void* /*data*/, const int /*sizeInBytes*/)
{
}