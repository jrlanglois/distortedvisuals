varying vec2 ex_TexCoord;

void main()
{
	// makes Pos have values of 1.0, 0.0 or -1.0
	vec2 Pos = sign (gl_Vertex.xy);
	
	// automatically set up texcoords
	ex_TexCoord = (Pos * 0.5) + 0.5;
	
	// give gl_Position the homogenous position
	gl_Position = vec4(gl_Vertex);
}