#include "MainComponent.h"

MainComponent::MainComponent() :
    transport (nullptr)
{
    setup.addAndConnectTransport (transport);
    transComp.setTransport (transport);
    this->addAndMakeVisible (&transComp);

	opengl.setBounds (0, 50, getWidth(), getHeight() - 50);

	this->addAndMakeVisible (&opengl);

	startTimer (1000 / 60);
}

MainComponent::~MainComponent()
{
}

//==============================================================================
void MainComponent::resized()
{
    transComp.setBounds (0, 0, this->getWidth(), 150);

	opengl.setBounds (0, 150, getWidth(), getHeight() - 150);
}                                  

////==============================================================================
void MainComponent::timerCallback()
{
	opengl.setScale (transport->getCurrentAmplitude());

	opengl.loop();
}