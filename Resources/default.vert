varying vec3 position;
varying vec3 normal;

void main()
{
    normal = gl_NormalMatrix * gl_Normal;

    vec4 hPos = gl_ModelViewMatrix * gl_Vertex;
    position = hPos.xyz;

    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}