#include "GraphicsComponent.h"

#include "gl3.h"

GraphicsComponent::GraphicsComponent()	: resizedAlready (false)
{
	context.setRenderer (this);
	context.setComponentPaintingEnabled (true);
	context.attachTo (*this);

	vertices = new float[72];
	normals = new float[72];
	
	// FACE 1
	vertices[0] = -1.0f;
	vertices[1] = 1.0f;
	vertices[2] = 1.0f;

	vertices[3] = -1.0f;
	vertices[4] = -1.0f;
	vertices[5] = 1.0f;

	vertices[6] = 1.0f;
	vertices[7] = -1.0f;
	vertices[8] = 1.0f;

	vertices[9] = 1.0f;
	vertices[10] = 1.0f;
	vertices[11] = 1.0f;

	// FACE 2
	vertices[12] = -1.0f;
	vertices[13] = 1.0f;
	vertices[14] = -1.0f;

	vertices[15] = 1.0f;
	vertices[16] = 1.0f;
	vertices[17] = -1.0f;

	vertices[18] = 1.0f;
	vertices[19] = -1.0f;
	vertices[20] = -1.0f;

	vertices[21] = -1.0f;
	vertices[22] = -1.0f;
	vertices[23] = -1.0f;

	// FACE 3
	vertices[24] = -1.0f;
	vertices[25] = -1.0f;
	vertices[26] = -1.0f;

	vertices[27] = 1.0f;
	vertices[28] = -1.0f;
	vertices[29] = -1.0f;

	vertices[30] = 1.0f;
	vertices[31] = -1.0f;
	vertices[32] = 1.0f;

	vertices[33] = -1.0f;
	vertices[34] = -1.0f;
	vertices[35] = 1.0f;

	// FACE 4
	vertices[36] = -1.0f;
	vertices[37] = 1.0f;
	vertices[38] = -1.0f;

	vertices[39] = -1.0f;
	vertices[40] = 1.0f;
	vertices[41] = 1.0f;

	vertices[42] = 1.0f;
	vertices[43] = 1.0f;
	vertices[44] = 1.0f;

	vertices[45] = 1.0f;
	vertices[46] = 1.0f;
	vertices[47] = -1.0f;

	// FACE 5
	vertices[48] = 1.0f;
	vertices[49] = 1.0f;
	vertices[50] = -1.0f;

	vertices[51] = 1.0f;
	vertices[52] = 1.0f;
	vertices[53] = 1.0f;

	vertices[54] = 1.0f;
	vertices[55] = -1.0f;
	vertices[56] = 1.0f;

	vertices[57] = 1.0f;
	vertices[58] = -1.0f;
	vertices[59] = -1.0f;

	// FACE 6
	vertices[60] = -1.0f;
	vertices[61] = 1.0f;
	vertices[62] = -1.0f;

	vertices[63] = -1.0f;
	vertices[64] = -1.0f;
	vertices[65] = -1.0f;

	vertices[66] = -1.0f;
	vertices[67] = -1.0f;
	vertices[68] = 1.0f;

	vertices[69] = -1.0f;
	vertices[70] = 1.0f;
	vertices[71] = 1.0f;

	// FACE 1
	normals[0] = 0.0f;
	normals[1] = 0.0f;
	normals[2] = 1.0f;

	normals[3] = 0.0f;
	normals[4] = 0.0f;
	normals[5] = 1.0f;

	normals[6] = 0.0f;
	normals[7] = 0.0f;
	normals[8] = 1.0f;

	normals[9] = 0.0f;
	normals[10] = 0.0f;
	normals[11] = 1.0f;

	// FACE 2
	normals[12] = 0.0f;
	normals[13] = 0.0f;
	normals[14] = -1.0f;

	normals[15] = 0.0f;
	normals[16] = 0.0f;
	normals[17] = -1.0f;

	normals[18] = 0.0f;
	normals[19] = 0.0f;
	normals[20] = -1.0f;

	normals[21] = 0.0f;
	normals[22] = 0.0f;
	normals[23] = -1.0f;

	// FACE 3
	normals[24] = 0.0f;
	normals[25] = -1.0f;
	normals[26] = 0.0f;

	normals[27] = 0.0f;
	normals[28] = -1.0f;
	normals[29] = 0.0f;

	normals[30] = 0.0f;
	normals[31] = -1.0f;
	normals[32] = 0.0f;

	normals[33] = 0.0f;
	normals[34] = -1.0f;
	normals[35] = 0.0f;

	// FACE 4
	normals[36] = 0.0f;
	normals[37] = 1.0f;
	normals[38] = 0.0f;

	normals[39] = 0.0f;
	normals[40] = 1.0f;
	normals[41] = 0.0f;

	normals[42] = 0.0f;
	normals[43] = 1.0f;
	normals[44] = 0.0f;

	normals[45] = 0.0f;
	normals[46] = 1.0f;
	normals[47] = 0.0f;

	// FACE 5
	normals[48] = 1.0f;
	normals[49] = 0.0f;
	normals[50] = 0.0f;

	normals[51] = 1.0f;
	normals[52] = 0.0f;
	normals[53] = 0.0f;

	normals[54] = 1.0f;
	normals[55] = 0.0f;
	normals[56] = 0.0f;

	normals[57] = 1.0f;
	normals[58] = 0.0f;
	normals[59] = 0.0f;

	// FACE 6
	normals[60] = -1.0f;
	normals[61] = 0.0f;
	normals[62] = 0.0f;

	normals[63] = -1.0f;
	normals[64] = 0.0f;
	normals[65] = 0.0f;

	normals[66] = -1.0f;
	normals[67] = 0.0f;
	normals[68] = 0.0f;

	normals[69] = -1.0f;
	normals[70] = 0.0f;
	normals[71] = 0.0f;

	// fullscreen quad
	quadVertices = new float[12];

	//T
	quadVertices[0] = -1.0f;
	quadVertices[1] = 1.0f;
	//
	quadVertices[2] = -1.0f;
	quadVertices[3] = -1.0f;
	//
	quadVertices[4] = 1.0f;
	quadVertices[5] = -1.0f;
	//T
	quadVertices[6] = -1.0f;
	quadVertices[7] = 1.0f;
	//
	quadVertices[8] = 1.0f;
	quadVertices[9] = -1.0f;
	//
	quadVertices[10] = 1.0f;
	quadVertices[11] = 1.0f;

	rotation = 0.0f;

	scale = 0.0f;
	scale2 = 0.0f;
	scale3 = 0.0f;
}

GraphicsComponent::~GraphicsComponent()
{
	// delete our handle
	glDeleteTextures(1, &texture);
	delete [] image;

	context.detach();

	delete [] vertices;
	delete [] normals;
	
	delete [] quadVertices;
}

//==============================================================================
void GraphicsComponent::resized()
{
	if (resizedAlready)
	{
		delete [] image;

		image = new unsigned char[getWidth() * getHeight() * 3];

		context.extensions.glActiveTexture (GL_TEXTURE0);

		glBindTexture (GL_TEXTURE_2D, texture);// bind our handle

		// set up filtering
	    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

		// set up a texture as RGB
		glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB, getWidth(), getHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	}

	resizedAlready = true;
}

//==============================================================================
void GraphicsComponent::loop()
{
	update();

	context.triggerRepaint();
}

//==============================================================================
void GraphicsComponent::newOpenGLContextCreated()
{
	shader = new OpenGLShaderProgram (context);
	
	juce::File vertShader (juce::File::getCurrentWorkingDirectory().getChildFile ("../../Resources/default.vert"));
	juce::File fragShader (juce::File::getCurrentWorkingDirectory().getChildFile ("../../Resources/default.frag"));

	juce::String vertText = vertShader.loadFileAsString();
	juce::String fragText = fragShader.loadFileAsString();

	shader->addShader (vertText.toUTF8(), GL_VERTEX_SHADER);
	shader->addShader (fragText.toUTF8(), GL_FRAGMENT_SHADER);

	shader->link();

	quadShader = new OpenGLShaderProgram (context);
	
	juce::File quadVertShader (juce::File::getCurrentWorkingDirectory().getChildFile ("../../Resources/quad.vert"));
	juce::File quadFragShader (juce::File::getCurrentWorkingDirectory().getChildFile ("../../Resources/quad.frag"));

	juce::String quadVertText = quadVertShader.loadFileAsString();
	juce::String quadFragText = quadFragShader.loadFileAsString();

	quadShader->addShader (quadVertText.toUTF8(), GL_VERTEX_SHADER);
	quadShader->addShader (quadFragText.toUTF8(), GL_FRAGMENT_SHADER);

	quadShader->link();

	glEnable (GL_CULL_FACE);
	
	glCullFace (GL_BACK);

	// create a renderbuffer for non antialiased scenes
	context.extensions.glGenRenderbuffers (1, &renderBuffer);
	context.extensions.glBindRenderbuffer (GL_RENDERBUFFER, renderBuffer);
	context.extensions.glRenderbufferStorage (GL_RENDERBUFFER, GL_DEPTH_COMPONENT, getWidth(), getHeight());

	// unbind buffers
	context.extensions.glBindRenderbuffer (GL_RENDERBUFFER, 0);

	// create a framebuffer
	context.extensions.glGenFramebuffers (1, &frameBuffer);
	context.extensions.glBindFramebuffer (GL_DRAW_FRAMEBUFFER, frameBuffer);

	glGenTextures (1, &texture);

	context.extensions.glActiveTexture (GL_TEXTURE0);

	glBindTexture (GL_TEXTURE_2D, texture);// bind our handle

	// set up filtering
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

	// set up a texture as RGB
	image = new unsigned char [getWidth() * getHeight() * 3];

    glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB, getWidth(), getHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, image);
}

void GraphicsComponent::openGLContextClosing()
{
}

void GraphicsComponent::renderOpenGL()
{
	glClearColor (1.0f, 1.0f, 1.0f, 1.0f);

	context.extensions.glBindFramebuffer (GL_DRAW_FRAMEBUFFER, frameBuffer);// set up to use a non antialiased framebuffer

	context.extensions.glFramebufferTexture2D (GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);
	context.extensions.glFramebufferRenderbuffer (GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, renderBuffer);

	glEnable (GL_DEPTH_TEST);

	shader->use();

	OpenGLHelpers::setPerspective (45.0, getWidth() / (double) getHeight(), 0.1, 100.0);

	glEnableClientState (GL_VERTEX_ARRAY);
    glVertexPointer (3, GL_FLOAT, 0, vertices);

	glEnableClientState (GL_NORMAL_ARRAY);
    glNormalPointer (GL_FLOAT, 0, normals);

	glTranslatef (0.0f, 0.0f, -5.0f);
	glRotatef (rotation, 0.0f, 1.0f, 0.0f);
	glScalef (0.5f + scale * 1.5f, 0.5f + scale * 1.5f, 0.5f + scale * 1.5f);
	
	glDrawBuffer (GL_COLOR_ATTACHMENT0);// only draw on colour attachment 0

	glClear (GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

    glDrawArrays (GL_QUADS, 0, 24);

	glVertexPointer (2, GL_FLOAT, 0, quadVertices);

	glDisableClientState (GL_NORMAL_ARRAY);

	context.extensions.glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

	glDisable (GL_DEPTH_TEST);

	glClear (GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	quadShader->use();

	int samplePos;
	int ampPos;

	context.extensions.glActiveTexture (GL_TEXTURE0);

	glBindTexture (GL_TEXTURE_2D, texture);// bind our handle

	ampPos = context.extensions.glGetUniformLocation (quadShader->programID, "amplitude");

	samplePos = context.extensions.glGetUniformLocation (quadShader->programID, "myTexture");

	context.extensions.glUniform1i (samplePos, 0);

	context.extensions.glUniform1f (ampPos, scale);

	glDrawArrays (GL_TRIANGLES, 0, 6);
}

void GraphicsComponent::update()
{
	rotation += 2.0f;
}

void GraphicsComponent::setScale (float newScale)
{
	scale2 = scale3;
	scale3 = scale4;
	scale4 = newScale;

	scale = (scale2 + scale3 + scale4) / 3.0f;
}