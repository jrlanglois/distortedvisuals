uniform float amplitude;

uniform sampler2D myTexture;

varying vec2 ex_TexCoord;

void main()
{
	float xOffset = 0.0;
	float yOffset = 0.0;

	float dist = length (ex_TexCoord - vec2 (0.5, 0.5));

	xOffset = (sin (dist * 100.0 * amplitude) * (ex_TexCoord.x - 0.5)) * 0.1 * amplitude;
	yOffset = (cos (dist * 100.0 * amplitude) * (ex_TexCoord.y - 0.5)) * 0.1 * amplitude;

	gl_FragColor = vec4 (textureLod (myTexture, vec2 (xOffset, yOffset) + ex_TexCoord, 0).rgb, 1.0);		
	//gl_FragColor = vec4 (textureLod (myTexture, ex_TexCoord.xy, 0).rgb, 1.0);
}